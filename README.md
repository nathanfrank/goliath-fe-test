Task one

using the form elements render all the form elements on the page. If the type is a "singleLine", it should be an input 
with the type of text.  If it is a multiLine is should be a text area.

task 2
when a user submits the form the data from the form should all be displayed in the area 
that says "Form Entries here".

task 3
when a user submits the form the data any element that has a "specialTemplateField" id should have its info
saved in its own object, this data should be rendered in the section that says "Special Template Values here" 
with the Name of the special template field associated with it.  Example. if the element has a specialTemplateFieldId of 
"123", and the special template field with the id of "123" has a name of "Full Name", and the user types "Ben Franklin"
the ui should display "Full Name: Ben Franklin"


export async function formService(url) {
    return data[url]
}

const data = {
    formData: [
        {
            id:"input1",
            label: "First and Last Name",
            type: "singleLine",
            specialTemplateId: "123"
        },
        {
            id:"input2",
            label: "First and Last Name",
            type: "multiLine"
        },
        {
            id: "input3",
            label: "Address",
            type: "singleLine",
            specialTemplateId: "456"
        }
    ],
    specialTemplateId: [
        {
            id: "123",
            name: "Full Name"
        },
        {
            id: '456',
            name: "address"
        }
    ]
}

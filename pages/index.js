import {useEffect, useState} from 'react';
import {formService} from "../api";

function HomePage() {

    const [formFields, updateFormFields] = useState([]);
    const [specialTemplateFields, updateSpecialTemplateFields] = useState([]);

    useEffect( () => {
        fetchFormFields()
        fetchSpecialTemplateFields()
    } )

    return (
        <div>
            <div>Form Here</div>

            <div>Form Entries here</div>

            <div>Special Template Values here</div>




        </div>
    )

    async function fetchFormFields() {
        updateFormFields(await formService('formData'))
    }

    async function fetchSpecialTemplateFields() {
        updateSpecialTemplateFields(await formService('specialTemplateId'))
    }
}



export default HomePage
